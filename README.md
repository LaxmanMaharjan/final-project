# Final Project
---
## Title: Sabaiko MIS

In present, Different schools and colleges have to purchase software to maintain their MIS system or the information of their students. 
Also the information of the students are not available to the students themself because the software is used by schools or colleges only.

What if the students wants to know:
- His Due amounts
- whether his information recorded by college is right or wrong.

So to solve some these problems, Sabaiko MIS is web platform where different schools or colleges can create MIS system for them and students can login to this platform and access the features of MIS system of their respective colleges like checking due amounts, whether his information is recorded correctly or not and etc.
___
Lots of Features can be added to this project in MIS level like:
- Student's Attendance
- Student's Results
- Student's Performance
 
Also the Schools and Colleges have their own profile so they can:
- post their notices
- photos of their schools programs like sports week, annual funcions and etc
- student can comment on those photos(posts) and all
- can also be made so that students from other schools or colleges can view, comments on posts.

_Also it can be the platform where students from different schools and colleges can meet, share knowledge, colab etc making this project highly extendable and scalable._
_It can be made engaging._


## How to Implemented it?
But for now, the project will be limited to checking due amount and student's information.

[Usecase Diagram](https://photos.app.goo.gl/Gfh9wvJ66WXcihUz9)

[ER Diagram](https://photos.app.goo.gl/R9mTKTWDFuEiMqy98)


### Level of Users:
- Students
- Teachers / College Admin
- System (Sabaiko MIS) Admin

Here to use this platform the colleges will have to register themself in this platform and system admin will verify the college through manual process since system admin have to check if the college is genuine college or not.

After the college is verified college will have email and password which is used as college admin. College Admin can perform action as shown in usecase diagram.
Also the college admin will provide the student email and password to students for logging in the platform.

