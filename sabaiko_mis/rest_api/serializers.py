from django.contrib.auth.models import User
from rest_framework import serializers
from mis_app.models import College, CollegeAdminUser, Student, WebsiteContent

class CollegeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = College
        fields = ['id','fullname','location','contact','email'] 

class WebsiteContentSerializer(serializers.ModelSerializer):

    class Meta:
        model = WebsiteContent
        fields = ['id','title','content']

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','username','password']
        extra_kwargs = {
                'password':{
                    'write_only':True,
                    'style':{
                        'input_type':'password'
                        }
                    }
                }

    def create(self, validated_data):
        user = User.objects.create_user(
                username= validated_data['username'],
                password= validated_data['password']
                )
        return user

class CollegeAdminUserSerializer(serializers.ModelSerializer):

    #user = serializers.PrimaryKeyRelatedField(queryset = User.objects.all())
    #college = serializers.PrimaryKeyRelatedField(queryset = User.objects.all())
    class Meta:
        model = CollegeAdminUser
        fields = ['id','user','college']

class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ['roll_no','fullname','age','contact','email','gender','father_name','father_contact','mother_name','mother_contact']

    def perform_create(self,serializer):
        serializer.save(college = self.request.user.admin.college)
        
