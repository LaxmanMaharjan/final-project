from rest_api.views import CollegeAdminUserViewSet, CollegeViewset, StudentViewset, UserViewset, WebsiteContentViewset
from django.urls import path, include
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'college',CollegeViewset)
router.register(r'websitecontent',WebsiteContentViewset)
router.register(r'user',UserViewset)
router.register(r'college_admin',CollegeAdminUserViewSet)
router.register(r'student',StudentViewset)
urlpatterns = [
        path('',include(router.urls)),
        ]
