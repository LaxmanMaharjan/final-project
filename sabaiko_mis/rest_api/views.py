from django.contrib.auth.models import User
from django.db.models import query
from mis_app.models import College, CollegeAdminUser, Student, StudentUser, WebsiteContent
from rest_api.serializers import CollegeAdminUserSerializer, CollegeSerializer, StudentSerializer, UserSerializer, WebsiteContentSerializer
from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.response import Response

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated

# Create your views here.
class CollegeViewset(viewsets.ModelViewSet):
    serializer_class = CollegeSerializer
    queryset = College.objects.all()
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

class WebsiteContentViewset(viewsets.ModelViewSet):
    serializer_class = WebsiteContentSerializer
    queryset = WebsiteContent.objects.all()
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

class UserViewset(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

class CollegeAdminUserViewSet(viewsets.ModelViewSet):
    serializer_class = CollegeAdminUserSerializer
    queryset = CollegeAdminUser.objects.all()
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAdminUser]

class StudentViewset(viewsets.ModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()
    authentication_classes = [BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def create_student_user(self, student, college):
        # for automatically creating student account just after creating adding student to college
        #last_name = student.fullname.split()[-1].lower()
        password = student.contact
        user = User(username = student.roll_no.lower(), password=password)
        user.set_password(password)
        user.save()
        student_account = StudentUser(user = user,student=student, college=college)
        student_account.save()
        #print('student user created')

    def create(self, request, *args, **kwargs):
        ##print('create student')
        ##print(request.data)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        college = request.user.admin.college
        data = request.data
        if 'csrfmiddlewaretoken' in data.keys():
            data.pop('csrfmiddlewaretoken')
        data['college'] = college
        #print(data)
        student = Student(**data)
        student.save()
        self.create_student_user(student,college)
        headers = self.get_success_headers(serializer.data)
        return Response("Student created successfully!!", status=status.HTTP_201_CREATED, headers=headers)


    def list(self, request):
        college = request.user.admin.college
        queryset = Student.objects.filter(college=college)
        serializer = StudentSerializer(queryset, many=True)
        return Response(serializer.data)
    
    def retrieve(self, request, *args, **kwargs):
        college = request.user.admin.college
        queryset = Student.objects.filter(college=college)
        
        student = get_object_or_404(self.queryset, pk=kwargs['pk'])
        serializer = StudentSerializer(student)
        return Response(serializer.data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        college = request.user.admin.college
        queryset = Student.objects.filter(college=college)
        
        student = get_object_or_404(self.queryset, pk=kwargs['pk'])

        serializer = self.get_serializer(student, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(serializer.data)

