from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.views.generic.base import TemplateView, View
from django.views.generic.detail import DetailView
from mis_app.forms import CollegeAdminUserForm
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic.edit import CreateView, DeleteView, FormView, UpdateView
from .models import Amount, AmountTime, College, CollegeAdminUser, CollegeImage, CollegeNotice, MainImage, Student, StudentUser, WebsiteContent, YearlyAmount
from django.views.generic import ListView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin


from .forms import  AmountForm, CollegeForm, CollegeImageForm, CollegeNoticeForm, MainImageForm, SearchForm, StudentForm, YearlyAmountForm

from django.http.response import HttpResponse, JsonResponse
from django.core import serializers

from weasyprint import HTML
from django.db.models import Sum

import tempfile
import datetime




# Create your views here.

def is_admin(username):
    admin_list = []
    for admin in CollegeAdminUser.objects.all():
        admin_list.append(admin.user.username)
    if username in admin_list:
        return True
    else:
        return False

class CollegeRegistration(CreateView):
    template_name = 'mis_app/form.html'
    form_class = CollegeForm
    success_url = 'registration_successfull'

class RegistrationSuccess(TemplateView):
    template_name = 'mis_app/registration_success.html'

class CollegeListView(ListView):
    model = College
    template_name = 'mis_app/college_list.html'
    queryset = College.objects.all().order_by('fullname')
    context_object_name = 'colleges' # by default it's name is object_list



class CollegeAdminUserFormView(FormView):
    form_class = AuthenticationForm
    template_name = 'mis_app/form.html'
    success_url = '/mis_app/student_dashboard'

    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(request, data= request.POST)
        if form.is_valid():

            username = form.cleaned_data.get('username').lower()
            password = form.cleaned_data.get('password')
            #print(username,password)
            if is_admin(username):
                self.success_url = '/mis_app/admin_dashboard'
                #print('user is admin')

            user = authenticate(username=username, password=password)
            if user is not None:
               login(request, user)
               messages.info(request, f"You are now logged in as {username}.")
            else:
               messages.error(request,"Invalid username or password.")
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

def logout_view(request):
    if is_admin(request.user.username):
        url = '/mis_app/'+str(request.user.admin.college.pk)+'/'+request.user.admin.college.fullname
    else:
        url = '/mis_app/'+str(request.user.student_user.college.pk)+'/'+request.user.student_user.college.fullname
    logout(request)
    return redirect(url)


class AdminDashboard(LoginRequiredMixin, View):
    redirect_field_name = 'admin_dashboard'
    def get(self, request):
        #print(request.user.admin.college)
        #print(type(request.user.admin))
        #print(type(request.user.admin.college))
        college = request.user.admin.college
        return render(request, 'mis_app/admin_dashboard.html',context={'college':college})

class StudentDashboard(LoginRequiredMixin, View):
    redirect_field_name = 'student_dashboard'
    def get(self, request):
        ##print(request.user.student_user)
        ##print(type(request.user.student_user))
        ##print(request.user.student_user.college)
        ##print(type(request.user.student_user.college))
        college = request.user.student_user.college
        return render(request, 'mis_app/student_dashboard.html',context={'college':college})

class Dashboard(View):
    def get(self, request):
        if is_admin(request.user.username):
            return redirect('admin_dashboard')
        else:
            return redirect('student_dashboard')
        
    

class StudentListView(LoginRequiredMixin, ListView): 
    model = Student
    template_name = 'mis_app/student_list.html'
    context_object_name = 'students' # by default it's name is object_list

    def get(self, request, *args, **kwargs):
        #print(request.user.admin)
        college = request.user.admin.college
        self.object_list = Student.objects.all().filter(college=college).order_by('roll_no')
        context = self.get_context_data()
        context['college']= college
        return self.render_to_response(context)

class CreateStudent(LoginRequiredMixin, View):

    def create_student_user(self, student, college):
        # for automatically creating student account just after creating adding student to college
        #last_name = student.fullname.split()[-1].lower()
        password = student.contact
        user = User(username = student.roll_no.lower(), password=password)
        user.set_password(password)
        user.save()
        student_account = StudentUser(user = user,student=student, college=college)
        student_account.save()
        #print('student user created')

    def get(self, request):
        return render(request, 'mis_app/student_create.html',{'form':StudentForm()})

    def post(self, request):
        #print('post')
        form = StudentForm(request.POST)
        if form.is_valid():
            data = request.POST.dict()
            #print(data)
            data.pop('csrfmiddlewaretoken')
            college = request.user.admin.college
            data['college'] = college
            #print(data)
            student = Student(**data)
            student.save()
            self.create_student_user(student,college)
            ser_instance = serializers.serialize('json', [student,]) # list of dictionary with necessary info
            #print(ser_instance)
            return JsonResponse({'instance': ser_instance,})
        else:
            #print('form invalid')
            messages.error(request,"Form Invalid")
            return JsonResponse({'instance': 'Form Invalid'})
            

class StudentUpdateView(LoginRequiredMixin, UpdateView):
    model = Student
    fields = ['fullname','age','contact','email','gender','father_name','father_contact','mother_name','mother_contact',]
    template_name = 'mis_app/student_update.html'

    def get_context_data(self, **kwargs):
        context =  super().get_context_data( **kwargs)
        context['id'] = self.object.pk
        return context

    def post(self, request, *args, **kwargs):
        #print(request.POST)
        #print(request.user.admin.college)
        # getting only required field for BlogCommentForm
        # roll_no to select specific student to be updated
        roll_no = kwargs['pk']
        student = Student.objects.get(pk=roll_no)
        data = request.POST.dict()
        data['roll_no'] = roll_no
        data['college'] = request.user.admin.college

        #print(student)
        form = StudentForm(data, instance=student)
        if form.is_valid():
            # Model.save() does either INSERT or UPDATE of an object in a DB, while Model.objects.create() does only INSERT.
            comment = form.save()
            ser_instance = serializers.serialize('json', [comment,]) # list of dictionary with necessary info
            return JsonResponse({'instance': ser_instance,})       
            #return JsonResponse({'instance': 'hello'})       

class StudentDeleteView(LoginRequiredMixin, DeleteView):
    
    def post(self, request, *args, **kwargs):
        #print(self.kwargs)
        pk = self.kwargs.get('pk')
        comment = get_object_or_404(Student,pk=pk)
        comment.delete()
        return JsonResponse({'Deleted': 'True'})       


class Home(TemplateView):
    template_name = 'mis_app/home.html'

    def get_context_data(self, **kwargs):
        image_obj = MainImage.objects.all().order_by('-time')
        content_obj = WebsiteContent.objects.all().order_by('-time')

        context =  super().get_context_data(**kwargs)
        # if because initially there is no image neither content
        if image_obj:
            context['image_obj'] = image_obj[0]
        
        if content_obj:
            context['content_obj'] = content_obj[0]
        
        return context

class AboutUs(TemplateView):
    template_name = 'mis_app/about_us.html'

class CollegeHome(View):
    def get(self, request,pk,college_name):
        ##print(college_name)
        college = get_object_or_404(College, pk=pk)
        #print(type(college))

        # when the college is initially created there will be no profile picture and notice 
        
        # so image and Notice may be None and indexing to None object lead to error so handling that case
        images = CollegeImage.objects.filter(college=college).order_by('-time')
        context = {'college':college, }
        if images:
            image_obj = images[0] 
            context['image_obj']=image_obj
        
        notice = CollegeNotice.objects.filter(college=college).order_by('-time')
        #print(notice)
        if notice:
            # display recent 5 notices
            notice_obj = notice[0:5]
            context['notice_obj']=notice_obj
        return render(request, 'mis_app/college_home.html', context=context)


# view for displaying image form view and uploading it and redirecting to college to home page
class CollegeImageView(LoginRequiredMixin,FormView):
    template_name = 'mis_app/form.html'
    form_class = CollegeImageForm

    def get(self, request, *args, **kwargs):
        self.extra_context = {'college':request.user.admin.college}
        
        return self.render_to_response(self.get_context_data())

    def form_valid(self, form):
        #print('form valid')
        #print(self.request.user.admin.college)
        image = form.cleaned_data.get('image')
        college = self.request.user.admin.college
        data = {'image': image, 'college':college}
        image_obj = CollegeImage(**data)
        image_obj.save()
        #print(image_obj)
        college = self.request.user.admin.college
        self.success_url = f'/mis_app/{college.pk}/{college.fullname}'
        return super().form_valid(form)

class CollegeNoticeView(LoginRequiredMixin, FormView):
    template_name = 'mis_app/form.html'
    form_class = CollegeNoticeForm
    
    def get(self, request, *args, **kwargs):
        self.extra_context = {'college':request.user.admin.college}
        
        return self.render_to_response(self.get_context_data())

    def form_valid(self, form):
        #print('form valid')
        #print(self.request.user.admin.college)
        title = form.cleaned_data.get('title')
        content = form.cleaned_data.get('content')
        college = self.request.user.admin.college
        data = {'title':title,'content':content ,'college':college}
        notice = CollegeNotice(**data)
        notice.save()
        #print(notice)
        self.success_url = f'/mis_app/{college.pk}/{college.fullname}'
        return super().form_valid(form)

# For presenting student's detail view
class StudentDetailView(DetailView):
    model = Student
    template_name = 'mis_app/student_detail.html'
    context_object_name = 'student'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        student = self.get_object()
        context['college'] = student.college
        return self.render_to_response(context)


class SearchView(LoginRequiredMixin, View):
    def post(self, request):
        #print('post')
        form = SearchForm(request.POST)
        if form.is_valid():
            roll_no = form.cleaned_data['roll_no']
            college = request.user.admin.college
            students = Student.objects.filter(college=college)
            required_student = None
            for student in students:
                if roll_no == student.roll_no:
                    required_student = student
            context = {}
            context['college']= college
            context['student']=required_student
            #print(required_student)
            #print(college)
            if required_student:
                return render(request,'mis_app/search_result.html',context)
            else:
                return JsonResponse({'Result':'Student Not found'})
        else:
            #print('form invalid')
            messages.error(request,"Form Invalid")
            return JsonResponse({'instance': 'Form Invalid'})

class DueAmountView(LoginRequiredMixin, View):
    def get(self,request):
        student = request.user.student_user.student
        college = request.user.student_user.college
        context = {}
        if hasattr(student,'amount'):
            context = {'due_amount':student.amount.due_amount,'paid_amount':student.amount.paid_amount, 'college':college}
        else:
            context = {'due_amount':college.yearly_amount.amount,'paid_amount':0,'college':college}
        return render(request,'mis_app/due_amount.html',context)

class CourseAmountView(LoginRequiredMixin, FormView):
    template_name = 'mis_app/form.html'
    form_class = YearlyAmountForm
    success_url = 'admin_dashboard'

    def get(self, request, *args, **kwargs):
        college = request.user.admin.college
        if hasattr(college,'yearly_amount'):
            return render(request,'mis_app/course_amount_response.html',{'college':college})

        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        amount = form.cleaned_data.get('amount')
        college = self.request.user.admin.college
        data = {'amount':amount,'college':college}
        yearly_amount = YearlyAmount(**data)
        yearly_amount.save()
        self.success_url = '/mis_app/admin_dashboard'
        return super().form_valid(form)

class CourseAmountResponseView(LoginRequiredMixin, TemplateView):
    template_name = 'mis_app/course_amount_response.html'

class AmountView(LoginRequiredMixin, FormView):
    template_name = 'mis_app/form.html'
    form_class = AmountForm
    
    def form_valid(self, form):
        roll_no = form.cleaned_data.get('roll_no').lower()
        paid_amount = form.cleaned_data.get('paid_amount')
        
        college = self.request.user.admin.college

        students = Student.objects.filter(college=college)
        student = None
        for stu in students:
            if roll_no == stu.roll_no:
                student = stu

        due_amount = 0
        if hasattr(student,'amount'):
            amount = student.amount
            due_amount = amount.due_amount - paid_amount
            total_paid_amount = amount.paid_amount + paid_amount
            amount.paid_amount = total_paid_amount 
            amount.due_amount  = due_amount
            amount.save(update_fields=['paid_amount','due_amount'])

        else:
            due_amount = college.yearly_amount.amount - paid_amount
            data = {'paid_amount':paid_amount,'due_amount':due_amount,'student':student}
            amount = Amount(**data)
            amount.save()

        # student has attribute amount_time even tho it's object is not created that's gg i don't know how but
        # that happens in ForeignKey so
        #if hasattr(student,'amount_time'):
            # saving amount for every time when the payment is done
            # in case of foreign key many to one relation select latest amount_time
        if student.amount_time.all():
            amount_time = student.amount_time.all().order_by('-time')[0]

            due_amount = amount_time.due_amount - paid_amount
            total_paid_amount = amount_time.paid_amount + paid_amount
            data = {'paid_amount':paid_amount,'due_amount':due_amount,'student':student}
            amount_time = AmountTime(**data)
            amount_time.save()

        else:
            due_amount = college.yearly_amount.amount - paid_amount
            data = {'paid_amount':paid_amount,'due_amount':due_amount,'student':student}
            amount_time = AmountTime(**data)
            amount_time.save()

        self.success_url = '/mis_app/admin_dashboard'
        return super().form_valid(form)

class PdfView(LoginRequiredMixin, View):
    def get(self, request):
        college = self.request.user.admin.college
        students = Student.objects.filter(college=college)
        context = {'students':[]}
        for student in students:
            if hasattr(student,'amount'):
                student_data = {'name':student.fullname,'paid_amount':student.amount.paid_amount,
                        'due_amount':student.amount.due_amount}
                context['students'].append(student_data)

        
        response = HttpResponse(content_type = 'application/pdf')
        response['Content-Dispostion'] = 'inline; attachment;filename:"Report.pdf"'
        response['Content-Transfer-Encoding'] = 'binary'
        html_string = render_to_string('mis_app/pdf_output.html',context)
        html = HTML(string=html_string)
        result = html.write_pdf() # binary
        ##print("Result",result)
        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output=open(output.name,'rb')
            response.write(output.read())
        return response
    
class StudentPdfView(LoginRequiredMixin, View):
    def get(self, request):
        student = request.user.student_user.student
        college = request.user.student_user.college
        amounts = student.amount_time.all()
        student_name = student.fullname.upper()
        context = {'amounts':amounts,'name':student_name}

        
        response = HttpResponse(content_type = 'application/pdf')
        response['Content-Dispostion'] = 'inline; attachment;filename:"Report.pdf"'
        response['Content-Transfer-Encoding'] = 'binary'
        html_string = render_to_string('mis_app/student_pdf.html',context)
        html = HTML(string=html_string)
        result = html.write_pdf() # binary
        ##print("Result",result)
        with tempfile.NamedTemporaryFile(delete=True) as output:
            output.write(result)
            output.flush()
            output=open(output.name,'rb')
            response.write(output.read())
        return response
#class MainImageView(LoginRequiredMixin,FormView):
#    template_name = 'mis_app/image_form.html'
#    model = MainImage
#    form_class = MainImageForm

