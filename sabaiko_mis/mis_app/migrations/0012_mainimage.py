# Generated by Django 3.2.12 on 2022-03-08 12:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mis_app', '0011_alter_studentuser_college'),
    ]

    operations = [
        migrations.CreateModel(
            name='MainImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='main_images')),
                ('time', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
