from django.db import models
from django import forms
from django.contrib.auth.models import User

# Create your models here.

class College(models.Model):
    fullname = models.CharField(max_length = 50)
    location = models.CharField(max_length = 50)
    contact = models.CharField(max_length = 10)
    email = models.EmailField()

    def __str__(self):
        return self.fullname

class CollegeImage(models.Model):
    # file will be uploaded to MEDIA_ROOT / images
    image = models.ImageField(upload_to = 'images')
    time = models.DateTimeField(auto_now=True)
    college = models.ForeignKey(College, on_delete=models.CASCADE, related_name='image')

    def __str__(self):
        return f"{self.image}"

class CollegeNotice(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    time = models.DateTimeField(auto_now=True)
    college = models.ForeignKey(College, on_delete=models.CASCADE, related_name='notice')

    def __str__(self):
        return self.title

class MainImage(models.Model):
    # image file to be display main website home (Sabaiko MIS)
    # file will be uploaded to MEDIA_ROOT / images
    image = models.ImageField(upload_to = 'main_images')
    time = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return f"{self.image}"

class CollegeAdminUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='admin')
    college = models.OneToOneField(College, on_delete=models.CASCADE, related_name = 'college')

    def __str__(self):
        return f"{self.user}:{self.college}"


class Student(models.Model):
    roll_no = models.CharField(max_length = 20 ,primary_key = True)
    fullname = models.CharField(max_length = 50)
    age = models.IntegerField()
    contact = models.CharField(max_length = 10)
    email = models.EmailField()
    gender_choices = [
            ('Male','Male'), #('Male','M')
            ('Female','Female')
            ]
    gender = models.CharField(
            max_length = 6,
            choices = gender_choices,
            default = 'Male'
            )
    father_name = models.CharField(max_length = 50)
    father_contact = models.CharField(max_length = 10)
    mother_name = models.CharField(max_length = 50)
    mother_contact = models.CharField(max_length = 10)
    #due_amount = models.IntegerField()
    college = models.ForeignKey(College, on_delete=models.CASCADE)

    def __str__(self):
        return self.fullname

class StudentUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student_user')
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name='student')
    college = models.ForeignKey(College, on_delete=models.CASCADE, related_name='colz')

    def __str__(self):
        return f"{self.user}:{self.college}"

class Teacher(models.Model):
    fullname = models.CharField(max_length = 50)
    age = models.IntegerField()
    contact = models.CharField(max_length = 10)
    email = models.EmailField()
    subject = models.CharField(max_length = 50)
    gender_choices = [
            ('M','Male'),
            ('F','Female')
            ]
    gender = models.CharField(
            max_length = 1,
            choices = gender_choices,
            default = 'M'
            )
    college = models.ForeignKey(College, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.fullname


class WebsiteContent(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

class YearlyAmount(models.Model):
    amount = models.IntegerField()
    college = models.OneToOneField(College, on_delete=models.CASCADE, related_name='yearly_amount')

    def __str__(self):
        return f"{self.college}:{self.amount}"

class Amount(models.Model):
    paid_amount = models.IntegerField(default=0)
    due_amount = models.IntegerField()
    student = models.OneToOneField(Student, on_delete=models.CASCADE, related_name='amount')
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.student}:{self.due_amount}"

class AmountTime(models.Model):
    paid_amount = models.IntegerField(default=0)
    due_amount = models.IntegerField()
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='amount_time')
    time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.student}:{self.due_amount}"


