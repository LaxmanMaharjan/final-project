from django.contrib import admin
from .models import Amount, AmountTime, College, CollegeImage, CollegeNotice, MainImage, Student, StudentUser, Teacher, CollegeAdminUser, WebsiteContent, YearlyAmount

# Register your models here.
@admin.register(College)
class CollegeAdmin(admin.ModelAdmin):
    list_display = ['fullname','location','contact','email']

@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    list_display = ['roll_no','fullname','contact','email']

@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ['fullname','contact','email']

@admin.register(CollegeAdminUser)
class CollegeAdminUserAdmin(admin.ModelAdmin):
    list_display = ['user','college']

@admin.register(StudentUser)
class StudentUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'college']

@admin.register(CollegeImage)
class CollegeImageAdmin(admin.ModelAdmin):
    list_display = ['image','time','college']

@admin.register(MainImage)
class MainImageAdmin(admin.ModelAdmin):
    list_display = ['image','time']

@admin.register(CollegeNotice)
class CollegeNoticeAdmin(admin.ModelAdmin):
    list_display = ['title','time','college']

@admin.register(WebsiteContent)
class WebsiteContentAdmin(admin.ModelAdmin):
    list_display = ['title','content','time']

@admin.register(YearlyAmount)
class YearlyAmountAdmin(admin.ModelAdmin):
    list_display = ['amount','college']

@admin.register(Amount)
class AmountAdmin(admin.ModelAdmin):
    list_display = ['student','paid_amount','due_amount','time']


@admin.register(AmountTime)
class AmountTimeAdmin(admin.ModelAdmin):
    list_display = ['student','paid_amount','due_amount','time']
