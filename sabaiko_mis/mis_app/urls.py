from mis_app.views import AdminDashboard, AmountView, CollegeAdminUserFormView, CollegeHome, CollegeImageView, CollegeListView, CollegeNoticeView, CollegeRegistration, CourseAmountResponseView, CourseAmountView, CreateStudent, Dashboard, DueAmountView, PdfView, RegistrationSuccess, SearchView, StudentDashboard, StudentDeleteView, StudentDetailView, StudentListView, StudentPdfView, StudentUpdateView, CourseAmountView, logout_view
from django.urls import path



urlpatterns = [
        path('colleges', CollegeListView.as_view(), name= 'college_list'),
        path('login', CollegeAdminUserFormView.as_view(), name= 'login'),
        path('logout', logout_view, name='logout'),
        path('dashboard', Dashboard.as_view(), name= 'dashboard'),
        path('admin_dashboard', AdminDashboard.as_view(), name= 'admin_dashboard'),
        path('student_dashboard', StudentDashboard.as_view(), name= 'student_dashboard'),
        path('<int:pk>/<str:college_name>', CollegeHome.as_view(), name= 'college_home'),
        path('create_student', CreateStudent.as_view(), name= 'create_student'),
        path('students', StudentListView.as_view(), name= 'student_list'),
        path('update_student/<str:pk>', StudentUpdateView.as_view(), name= 'update_student'),
        path('delete_student/<str:pk>', StudentDeleteView.as_view(), name= 'delete_student'),
        path('college_registration', CollegeRegistration.as_view(), name= 'college_registration'),
        path('registration_successfull', RegistrationSuccess.as_view(), name= 'successfull'),


        path('upload_image', CollegeImageView.as_view(), name= 'upload_image'),
        path('upload_notice', CollegeNoticeView.as_view(), name= 'upload_notice'),


        path('student_detail/<str:pk>', StudentDetailView.as_view(), name= 'student_detail'),
        path('search', SearchView.as_view(), name= 'search'),
        path('due_amount', DueAmountView.as_view(), name= 'due_amount'),
        path('course_amount', CourseAmountView.as_view(), name= 'course_amount'),
        path('amount', AmountView.as_view(), name= 'amount'),
        path('course_amount_response', CourseAmountResponseView.as_view(), name= 'amount_response'),
        path('pdf', PdfView.as_view(), name= 'pdf'),
        path('student_pdf', StudentPdfView.as_view(), name= 'student_pdf'),

        ]
