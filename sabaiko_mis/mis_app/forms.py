from django.contrib.auth.models import User
from .models import Amount, CollegeImage, CollegeNotice, MainImage, Student, College, Teacher, CollegeAdminUser, YearlyAmount
from django import forms

class CollegeForm(forms.ModelForm):
    class Meta:
        model = College
        fields = '__all__'

class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['roll_no','fullname','age','contact','email','gender','father_name','father_contact','mother_name','mother_contact',]

class CollegeAdminUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username','password']

class CollegeImageForm(forms.ModelForm):
    class Meta:
       model = CollegeImage 
       fields = ['image']

class CollegeNoticeForm(forms.ModelForm):
    class Meta:
        model = CollegeNotice
        fields = ['title','content']
        widgets = {
            'content': forms.Textarea(attrs={'cols': 20, 'rows': 10}),
        }

class MainImageForm(forms.ModelForm):
    class Meta:
       model = MainImage
       fields = ['image']

class SearchForm(forms.Form):
    roll_no = forms.CharField(max_length=20)

class YearlyAmountForm(forms.ModelForm):

    class Meta:
        model = YearlyAmount
        fields = ['amount']

class AmountForm(forms.ModelForm):

    roll_no = forms.CharField(max_length=20)
    class Meta:
        model = Amount
        fields = ['paid_amount',]
